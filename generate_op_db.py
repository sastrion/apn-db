#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import datetime
import sys

country_codes = ['ro', 'ru', 'hr', 'rs', 'tr', 'gr', 'at', 'it', 'fr', 
                 'nl', 'uk', 'ie', 'ua', 'pl', 'by', 'ee', 'lt', 'lv', 
                 'es', 'pt', 'si', 'bg', 'cz', 'sk', 'de', 'lt', 'hu',
                 'mx', 'us', 'ca']

tree = ET.parse('serviceproviders.xml')
root = tree.getroot()
db = {}
providers = {}

for country in root:
	cc = country.get('code')
	if (not cc in country_codes):
		continue
	
	for provider in country:
		primary = provider.get('primary')
		if (primary == 'false'):
			continue
		
		name = provider.find('name').text
		gsm = provider.find('gsm')
		if (gsm is None):
			continue
		
		apn_name = ''
		apn_user = ''
		apn_passwd = ''

		for apn in gsm.findall('apn'):
			usage = apn.find('usage')
			if (usage is None):
				continue
			if (usage.get('type') == "internet"):
				apn_name = apn.get('value')
				if (apn.find('username') is not None):
					apn_user = apn.find('username').text
				if (apn.find('password') is not None):
					apn_passwd = apn.find('password').text
				break
		
		if not apn_name:
			continue

		balance_check = gsm.find('balance-check')
		if (balance_check is not None):
			ussd = balance_check.find('ussd')
			if (ussd is not None):
				ussd = ussd.text
			else:
				ussd = ''
		else:
			ussd = ''
		
		net_ids = gsm.findall('network-id')
		for net_id in net_ids:
			pid = net_id.get('mcc') + net_id.get('mnc')
			if (pid in db):
				continue
			
			cfg = {'cc' : cc, 'name' : name, 'id' : pid, 'apn' : apn_name, 'user' : apn_user, 'pass' : apn_passwd, 'ussd' : ussd}
			key = (cfg['cc'], cfg['name'])
			if (not key in providers):
				providers[key] = cfg;
		
			if ((apn_name, apn_user, apn_passwd) == ('internet', '', '') and ussd in ['', '*100#']):
				continue;
			
			db[pid] = cfg

if (len(sys.argv) > 1 and sys.argv[1] == 'list'):
	print('cc,name,id,apn,user,pass,ussd')
	for k in sorted(providers):
		v = providers[k]
		print('{},{},{},{},{},{},{}'.format(v['cc'], v['name'], v['id'], v['apn'], v['user'], v['pass'], v['ussd']))
else:
	print('# Created on {}'.format(datetime.datetime.now()))
	for k, v in db.items():
		print('{},{},{},{},{}'.format(k, v['apn'], v['user'], v['pass'], v['ussd']))
